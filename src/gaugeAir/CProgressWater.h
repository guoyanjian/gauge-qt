#ifndef CPROGRESSWATER_H
#define CPROGRESSWATER_H

#include <QWidget>
#include <QPainter>
#include <QtMath>
#include <QDebug>

namespace Ui {
class CProgressWater;
}

class CProgressWater : public QWidget
{
    Q_OBJECT

public:
    explicit CProgressWater(QWidget *parent = 0);
    ~CProgressWater();
    enum PercentStyle_Type{
        PercentStyle_Rect = 0,
        PercentStyle_Circle,
        PercentStyle_Ellipse,
    };

    void setValue(int value); // {m_value = value;}
    int  getValue(); // {return m_value;}

    void setMaxValue(int value); // {m_maxValue = value;}
    int  getMaxValue(); // {return m_maxValue;}

    void setMinValue(int value); // {m_minValue = value;}
    int  getMinValue(); // {return m_minValue;}

    void setPercentStyle(PercentStyle_Type type); // {m_percentStyle = type;}
    PercentStyle_Type  getPercentStyle(); // {return m_percentStyle;}

    void setWaterDensity(int value); // {m_waterDensity = value;}
    int  getWaterDensity(); // {return m_waterDensity;}

    void setColor(QColor color); //  {m_usedColor = color;}
    QColor  getColor(); //  {return m_usedColor;}

    void setWaterHeight(double value); // {m_waterHeight = value;}
    double  getWaterHeight(); //  {return m_waterHeight;}

    void setBorderWidth(int value); //  {m_borderWidth = value;}
    int  getBorderWidth(); //  {return m_borderWidth;}

    void setTextColor(QColor color); //  {m_textColor = color;}
    QColor  getTextColor(); //  {return m_textColor;}

    void setBoderColor(QColor color); //  {m_boderColor = color;}
    QColor  getBoderColor(); //  {return m_boderColor;}

    void setBgColor(QColor color); //  {m_bgColor = color;}
    QColor  getBgColor(); // {return m_bgColor;}

protected:
    void paintEvent(QPaintEvent *event);
    void timerEvent(QTimerEvent *event);
    void drawBg(QPainter *painter);
    void drawProgress(QPainter *painter);
    void drawValue(QPainter *painter);

private:
    Ui::CProgressWater *ui;

    PercentStyle_Type m_percentStyle; // = PercentStyle_Circle;
    QColor m_usedColor; // = QColor(180, 255, 255);
    QColor m_textColor; // = Qt::white;
    QColor m_boderColor; // = Qt::black;
    QColor m_bgColor; // = Qt::gray;
    QColor m_outlineColor;

    QFont m_font;
    int m_value; // = 80;
    int m_minValue; // = 0;
    int m_maxValue; // = 100;
    int m_waterDensity; // = 6; // 水波的密度
    double m_waterHeight; // = 0.03;
    double m_offset; // = 50;
    int m_borderWidth; // = 10;

};

#endif // CPROGRESSWATER_H
